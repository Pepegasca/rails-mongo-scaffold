# Aplicación Rails Scaffold

```bash
rails new mongo-bootstrap --skip-active-record
```
Agregamos mongoid al archivo `Gemfile`

```ruby
gem 'mongoid'
```

Instalamos 

```bash
bundle install
```

Creamos el modelo, controlador y vistas usando `rails generate scaffold`,

```bash
rails g scaffold blog_entry title:string description:text
```

con esto iniciamos nuestro servidor y tendremos la aplicación corriendo en `http://localhost:3000`.



